import classes.Book;
import classes.Library;

public class Main {
    public static void main(String[] args) {
        Library library = new Library();
        library.addBook(new Book("a1", "author1", 1999, "a1-author1"));
        library.addBook(new Book("a2", "author1", 2000, "a2-author1"));
        library.addBook(new Book("a3", "author1", 2001, "a3-author1"));
        library.addBook(new Book("b1", "author2", 1999, "b1-author2"));
        library.addBook(new Book("b2", "author2", 2010, "b2-author2"));
        library.addBook(new Book("b3", "author2", 1992, "b3-author2"));
        library.addBook(new Book("c1", "author3", 2020, "c1-author3"));
        library.addBook(new Book("c2", "author3", 2018, "c2-author3"));

        System.out.println("authors of all books published before 2005: " + library.getAuthorsOfBooksPublishedBeforeYear(2005));

        System.out.println();

        System.out.println("before deletion: ");
        System.out.println("books published after 2017:");
        for(Book book: library.getBooksPublishedAfterYear(2017)){
            System.out.println(book.getBookInfo());
        }

        System.out.println();

        library.removeBook("c1-author3");
        System.out.println("after deletion: c1-author3");
        for(Book book: library.getBooksPublishedAfterYear(2017)){
            System.out.println(book.getBookInfo());
        }

    }
}